const express = require("express") ;
const bodyParser = require("body-parser") ;
const mongoose = require("mongoose") ; 
const Registeration = require("./models/registraion") ; 


const app = express() ; 
app.use(bodyParser.json()) ; 
app.use(bodyParser.urlencoded({ extended: false }));

mongoose.connect("mongodb://localhost:27017/nanoTechnology-Faculty")
.then(() => {
   console.log("Connected to database!");
 })
 .catch(() => {
   console.log("Connection failed!");
 });

 // this for solving cors problem 
// to servers one for frontend on localhost:4200 , second for backend on localhost:3000
app.use((req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin" , "*") ; 
    res.setHeader("Access-Control-Allow-Headers" , 
                 "Origin , X-Request-With , Content-Type , Accept" ); 
    res.setHeader("Access-Control-Allow-Methods" ,
                 "GET , POST , PATCH , DELETE , OPTIONS" ) ; 
                
    next() ;                 
})

app.get("", (req,res,next)=>{
  
})
app.get("", (req,res,next)=>{
  
})

module.exports = app ; 