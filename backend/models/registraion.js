const mongoose = require('mongoose');

const registerSchema = mongoose.Schema({
    ArabicName: { type: String, required: true, },
    EnglishName: { type: String, required: true },
    NationalId: { type: Number, required: true },
    NationalID_issuer: { type: String, required: true },
    NationalId_Date: { type: Date, required: true },
    Country: { type: String, required: true },
    Gender: { type: String, required: true },
    Military_Service: { type: String, required: true },
    Military_Service_date: { type: Date, required: true },
    RegisteredBefore: { type: Boolean, required: true }
});

module.exports = mongoose.model('Registeration', registerSchema);
