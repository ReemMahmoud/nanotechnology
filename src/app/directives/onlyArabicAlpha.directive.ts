import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[arabicAlphaOnly]'
})
export class OnlyArabicAlphaDirective {

  constructor(private _el: ElementRef) { }

  @HostListener('input', ['$event']) onInputChange(event) {
    const initalValue = this._el.nativeElement.value;
  
 this._el.nativeElement.value = initalValue.replace(/[u0600-u06FF]/, '');

    if ( initalValue !== this._el.nativeElement.value) {
      event.stopPropagation();
    }
  }
}