import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[numbersOnly]'
})
export class OnlynumberDirective {

  constructor(private _el: ElementRef) { }

  @HostListener('input', ['$event']) onInputChange(event) {
    const initalValue = this._el.nativeElement.value;
   //[0-9]*
  this._el.nativeElement.value = initalValue.replace(/[^0-9]/, '');
 

    if ( initalValue !== this._el.nativeElement.value) {
      event.stopPropagation();
    }
  }
}