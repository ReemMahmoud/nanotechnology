import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from "@angular/forms";
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { OnlynumberDirective } from '../app/directives/onlynumber.directive';
import { OnlyEnglishAlphaDirective } from "../app/directives/onlyEnglishAlpha.directive";
import { OnlyArabicAlphaDirective } from "../app/directives/onlyArabicAlpha.directive";

import {

  MatToolbarModule, MatIconModule, MatSidenavModule, MatListModule, MatButtonModule, MatSelectModule
  , MatCardModule, MatInputModule, MatRadioModule, MatNativeDateModule,
  MatCheckboxModule

} from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatTabsModule} from '@angular/material/tabs';
import { ContentComponent } from './content/content.component';
import { HeaderComponent } from "./header/header.component";
import { RegistrationFormComponent } from './registration-form/registration-form.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    RegistrationFormComponent,
    OnlynumberDirective,
    OnlyEnglishAlphaDirective,
    OnlyArabicAlphaDirective,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatCheckboxModule,
    MatTabsModule

  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
