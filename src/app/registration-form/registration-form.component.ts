import { Component, OnInit } from '@angular/core';
import { FormGroup , FormControl, Validators } from '@angular/forms';
import { MatRadioChange } from '@angular/material';
import { RegisterFormService } from "./registration-form.service"
import { Helper } from "../models/helper.model";


@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})

export class RegistrationFormComponent implements OnInit {

  private programs: Helper[] = [];
  private genders : Helper[] = [];
  private militrayServices : Helper[] = [] ; 
  private countriesOfBirth : Helper[] = [] ; 
  private type : any ;
  private showInputs : Boolean = false ; 
  private form : FormGroup ; 
  private imagePreview : string ;
  private errorArabicMessage : boolean = false ;
  private errorEnglishMessage : boolean = false ;
  private idErrorMessage : boolean = false ;
  constructor(public registrationService: RegisterFormService) {}

  ngOnInit() {

    this.form = new FormGroup({
      arabicName: new FormControl("", { validators: [Validators.required ]}),
      englishName: new FormControl("", { validators: [Validators.required] }),
      nationalId: new FormControl("", { validators: [Validators.required] }),
      nationalIdCountry: new FormControl("", { validators: [Validators.required] }),
      nationalIdDate :new FormControl(new Date()),
      countryOfBirth :new FormControl("", { validators: [Validators.required] }),
      gender :new FormControl("", { validators: [Validators.required] }),
      registerBefore :new FormControl("", { validators: [Validators.required] }),
      DateOfBirth :new FormControl(new Date()),
      militOption:new FormControl("", { validators: [Validators.required] }) ,
      image: new FormControl("", {validators: [Validators.required]})  , 
      militrayServiceDate : new FormControl(new Date())
   
  });

    this.militrayServices = [ 
    {Id: 1, Name: "مؤجل"} ,
    {Id: 1, Name: "معافى"}
    ]
    this.countriesOfBirth = [
      {Id: 1, Name: "مصر"} ,
      {Id: 2, Name: "السودان"},
      {Id: 3, Name: "ليبيا"} ,
      {Id: 4, Name: "تونس"} ,
      {Id: 5, Name: "الجزائر"} ,
      {Id: 6, Name: "المغرب"} ,
      {Id: 7, Name: "موريتانيا"} ,
      {Id: 8, Name: "الاردن"} ,
      {Id: 9, Name: "سوريا"} ,
      {Id: 10, Name: "لبنان"} ,
      {Id: 11, Name: "فلسطين"} ,
      {Id: 12, Name: "العراق"} ,
      {Id: 13, Name: "الامارات"} ,
      {Id: 14, Name: "البحرين"} ,
      {Id: 15, Name: "الكويت"} ,
      {Id: 16, Name: "السعودية"} , 
      {Id: 17, Name: "عمان"} ,
      {Id: 18, Name: "قطر"} ,
      {Id: 19, Name: "اليمن"} ,
      {Id: 20, Name: "الصومال"} ,
      {Id: 21, Name: "جيبوتى"} ,

    ]
    this.registrationService.getPrograms()
      .subscribe((programs: Helper[]) => {
        this.programs = programs["program"]
      })

    this.registrationService.getGenders()
      .subscribe((genders: Helper[]) => {
        this.genders = genders["gender"]
      })

  }
  writeName(name : string){
    if(name == "arabicName"){
      var inputValue =  this.form.value.arabicName.trim().split(/\s+/);
      var num_words = inputValue.length;
      if(num_words < 3){
         this.errorArabicMessage = true ;
      }
      if(num_words >= 3){
         this.errorArabicMessage = false ;
      }
    }else if (name == "englishName"){
      
        var inputValue =  this.form.value.arabicName.trim().split(/\s+/);
        var num_words = inputValue.length;
        if(num_words < 3){
           this.errorEnglishMessage = true ;
        }
        if(num_words >= 3){
           this.errorEnglishMessage = false ;
        }
    }else{
        var id_length =  this.form.value.nationalId.length ;
        if(id_length >14 || id_length <14){
         this.idErrorMessage = true ;
        }else{
          this.idErrorMessage = false ;
        }
        
    }
   
  }
 
  addRegistration(){
    console.log("form" , this.form.value) ; 
    
  }
  radioChange(event: MatRadioChange, data) {
   
    this.type  = event.value;
  
    if(this.type == "ذكر"){
      this.showInputs = true 
    }
  }
  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({ image: file });
    this.form.get("image").updateValueAndValidity();
    console.log("image is ::", file) ; 
    console.log("form is ::" , this.form) ; 
    const reader = new FileReader();
    reader.onload = () => {
        
      this.imagePreview = reader.result as string;
      console.log("this.form.value.image" ,this.form.value.image) ; 
    };
    reader.readAsDataURL(file);
   
  }


}


