import { Injectable } from '@angular/core';
import { Helper } from "../models/helper.model" ; 
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http" ; 




@Injectable({providedIn : 'root'})
export class RegisterFormService {
    constructor(private http : HttpClient){}
    private programs : Helper[] = []  ; 

    getPrograms() : Observable<Helper[]>{
      return this.http
                 .get<Helper[]>('http://34.251.248.109/API/Helper?type="Program"')
                
    }
    getGenders() : Observable<Helper[]>{
      return this.http
                 .get<Helper[]>('http://34.251.248.109/API/Helper?type="GenderId"')
                
    }
  

}